package common;

public class Config {
    public static final String PLATFORM_AND_BROWSER = "win_chrome";

    /*
    Clear browser cookies after each iteration
    if "true" - clean
     */
    public static final Boolean CLEAR_COOKIES_AND_STORAGE = true;

    /*
    Close browser after suite
    if "true" - close
     */
    public static final Boolean CLOSE_BROWSER = true;
}
