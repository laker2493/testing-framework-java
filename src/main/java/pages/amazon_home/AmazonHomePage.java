package pages.amazon_home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.base.BasePage;

public class AmazonHomePage extends BasePage {

    public AmazonHomePage(WebDriver driver) {
        super(driver);
    }
    /*
    Sets locators
    */
    private final By searchDepartment = By.id("searchDropdownBox");
    private final By optionsElectronics = By.xpath("//*[@id='searchDropdownBox']/option[29]"); //Electronics department
    private final By searchBtn = By.id("nav-search-submit-button");

    /*
    Sets department
    */
    public AmazonHomePage enterDepartment(){
        driver.findElement(searchDepartment).click();
        driver.findElement(optionsElectronics).click();
        return this;
    }

    /*
    Clicks on search button
    */
    public AmazonHomePage clickSearchButton() {
        WebElement btnSearch = driver.findElement(searchBtn);
        waitElementIsVisible(btnSearch).click();
        return this;
    }
}
