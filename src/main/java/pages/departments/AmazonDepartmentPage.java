package pages.departments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pages.base.BasePage;

public class AmazonDepartmentPage extends BasePage {

    public AmazonDepartmentPage (WebDriver driver) {
        super(driver);
    }

    private final By category = By.xpath("//*[@id='s-refinements']/div[1]/ul/li[6]/span/a/span");


    public AmazonDepartmentPage checkIfCategoryExists(){
        Assert.assertNotNull(category);
        return this;
    }
}
