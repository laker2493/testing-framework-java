package tests.base;

import common.CommonActions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import pages.amazon_home.AmazonHomePage;
import pages.base.BasePage;
import pages.departments.AmazonDepartmentPage;

import static common.Config.CLEAR_COOKIES_AND_STORAGE;
import static common.Config.CLOSE_BROWSER;

public class BaseTest {
    protected WebDriver driver = CommonActions.createWebDriver();
    protected BasePage basePage = new BasePage(driver);
    protected AmazonHomePage amazonHomePage = new AmazonHomePage(driver);
    protected AmazonDepartmentPage amazonDepartmentPage = new AmazonDepartmentPage(driver);

    /*
    Clean cookies and storage after iteration
     */
    @AfterTest
    public void clearCookiesAndLocalStorage(){
        if(CLEAR_COOKIES_AND_STORAGE) {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            driver.manage().deleteAllCookies();
            javascriptExecutor.executeScript("window.sessionStorage.clear()");
        }
    }

    /*
    Close browser after suite
     */
    @AfterSuite (alwaysRun = true)
    public void close(){
        if(CLOSE_BROWSER){
            driver.quit();
        }
    }

}
