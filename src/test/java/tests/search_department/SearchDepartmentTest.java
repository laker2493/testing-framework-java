package tests.search_department;

import org.testng.annotations.Test;
import tests.base.BaseTest;

import static constants.Constant.Urls.AMAZON_HOME_PAGE;

public class SearchDepartmentTest extends BaseTest {

    @Test
    public void checkIsRedirectToDepartment(){
        basePage.open(AMAZON_HOME_PAGE);

        amazonHomePage
                .enterDepartment()
                .clickSearchButton();

        amazonDepartmentPage.checkIfCategoryExists();
    }
}
